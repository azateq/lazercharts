
var pair = document.title;

var id2 = "Bitfinex_amount"
var buf2 = {};
buf2['Bitfinex_amount'] = [[], []];

var ws2 = new WebSocket('wss://api.bitfinex.com/ws/');


ws2.onopen = function() {
    ws2.send(JSON.stringify({      // send subscribe request
        "event": "subscribe",
        "channel": "trades",
        "pair": pair
    }));
};


ws2.onmessage = function(msg) {     // callback on message receipt
    var response = JSON.parse(msg.data);
    var tempvar;
    globaltime = response[3] * 1000;
    if (response[1] === 'te') {    // Only 'te' message type is needed

        tempvar = response[5] * 5
        if(tempvar < 0){
            tempvar = response[5] * -1;
        }
        if(tempvar > 15){
            tempvar = 15;
        }
        buf2['Bitfinex_amount'][response[5] > 0 ? 0 : 1].push({
            x: response[3] * 1000, // timestamp in milliseconds
            y: response[5],   // amountr
            r: tempvar
        });
    }
}


var ctx = document.getElementById(id2).getContext('2d');

var chart = new Chart(ctx, {

    type: 'bubble',

    data: {

        datasets: [{
            data: [],
            label: 'Buy',
            spanGaps: false,
            cubicInterpolationMode: 'default',
            borderColor: 'red',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
            fill: false,
            lineTension: 1,                     // straight line
            pointRadius: 0                      // no point
        },{
            data: [],
            label: 'Sell',
            borderColor: 'rgb(54, 162, 235)',
            backgroundColor: 'rgba(54, 162, 235, 0.5)',
            fill: false,
            lineTension: 0,                    // straight line
            pointRadius: 0                     // no point

        }]

    },


    options: {
        title: {
            text: 'BTC/USD (' + id2 + ')', // chart title
            display: true
        },
        scales: {
            xAxes: [{
                type: 'realtime' // auto-scroll on X axis
            }],
            yAxes: [{
                position: 'right',

            }]
        },
        plugins: {
            streaming: {
                duration: 30000, // display data for the latest 300000ms (5 mins)
                onRefresh: function(chart) { // callback on chart update interval
                    Array.prototype.push.apply(
                        chart.data.datasets[0].data, buf2[id2][0]
                    );            // add 'buy' price data to chart
                    Array.prototype.push.apply(
                        chart.data.datasets[1].data, buf2[id2][1]
                    );
                    // add 'sell' price data to chart
                    buf2[id2] = [[], []]; // clear buffer
                },
                delay: 100
            }
        }

    }})


