
var lastprice;

function btcusdprice(){
    "use strict";
    $('.btcusd-pair').text(btcpair);
    $('.btcusd-price').text(price.toFixed(2) + " " + currency);
    if(price => lastprice){
        $('.heatmap-btc').toggleClass('heatmap-posneg heatmap-negative')
    }
    if(lastChangePcnt>=0){
        $('.btcusd-lastChangePcnt').text((lastChangePcnt*100).toFixed(2) + "%").toggleClass('heatmap-posneg positive')
    }
    lastprice = price;
}

function ltcusdprice(){
    "use strict";
         $('.ltcusd-pair').text(ltcpair);
        $('.ltcusd-price').text(price.toFixed(2) + " " + currency);
    if(price => lastprice){
        $('.heatmap-ltc').toggleClass('heatmap-posneg heatmap-negative')
    }
    if(lastChangePcnt>=0){
        $('.ltcusd-lastChangePcnt').text((lastChangePcnt*100).toFixed(2) + "%").toggleClass('heatmap-posneg positive')
    }
    lastprice = price;
}

function ethusdprice(){
    "use strict";
    $('.ethusd-pair').text(ethpair);
    $('.ethusd-price').text(price.toFixed(2) + " " + currency);
    if(price => lastprice){
        $('.heatmap-eth').toggleClass('heatmap-posneg heatmap-negative')
    }
    if(lastChangePcnt>=0){
        $('.ethusd-lastChangePcnt').text((lastChangePcnt*100).toFixed(2) + "%").toggleClass('heatmap-posneg positive')
    }
    lastprice = price;
}

function trades(time, price, amount){
    /*
     var oneDateMiliseconds = time.getTime();
        var currentMiliseconds = Date.now();
        var timePassedInMilliseconds = (currentMiliseconds-oneDateMiliseconds)/1000;
        console.log(timePassedInMilliseconds)
            <div class="trade-item">
                <div class="trade-col1">updown</div>
                <div class="trade-col2">price</div>
                <div class="trade-col3">amount</div>
            </div>

     */
    var tradeupdownicon = "<i class='fa fa-chevron-up fa-blank'></i>"

    var updown;
    var tradeupdown;
    price = price.toFixed(2);
    amount = amount.toFixed(5);
    var contentstyle = "<span class=''></span>";
    var tradeFeedVolumeFilter = $('#tradeFeedVolumeFilter').val();


    if(price > lastprice){
        tradeupdownicon = "<i class='fas fa-chevron-up'></i>"
    }else if(price < lastprice){
        tradeupdownicon = "<i class='fas fa-chevron-down'></i>"

    }
    if (amount > tradeFeedVolumeFilter || amount < -tradeFeedVolumeFilter) {
        var i = 0;
        var contentLength = $(".trades-btc > div").length;

        var clearfix = "<div class='clearfix'></div>"
        var contentupdown = "<div class='trade-col1'>"+tradeupdownicon +"</div>"
        var contentprice = "<div class='trade-col2'>"+price+"</div>"
        var contentamount = "<div class='trade-col3'>"+amount+"</div>"




        var tradeitem = "<div class='trade-item'>"+contentstyle+contentupdown + contentprice + contentamount+clearfix+"</div>"
        $(".trades-btc").prepend(tradeitem)

        if(amount > tradeFeedVolumeFilter){
            $('.trade-item:first-child').css('background-color', 'rgba(0, 128, 0,' + amount);
        }else if(amount < -tradeFeedVolumeFilter){
            amount = amount * -1
            $('.trade-item:first-child').css('background-color', 'rgba(255, 0, 2,' + amount);
        }

        var lasttradeslength = $('#tradeFeedLengthFilter').val();
        if(contentLength > lasttradeslength){
            var $ul = $(".trades-btc")
            $ul.find('div').slice(-lasttradeslength).remove()
        }
        lastprice = price;
        i = i + 1;

    }

}

function btcusdtrades(time, price, amount){
/*
 var oneDateMiliseconds = time.getTime();
    var currentMiliseconds = Date.now();
    var timePassedInMilliseconds = (currentMiliseconds-oneDateMiliseconds)/1000;
    console.log(timePassedInMilliseconds)
        <div class="trade-item">
            <div class="trade-col1">updown</div>
            <div class="trade-col2">price</div>
            <div class="trade-col3">amount</div>
        </div>

 */
    var tradeFeedVolumeFilter = $('#tradeFeedVolumeFilter').val();
    if (amount > tradeFeedVolumeFilter || amount < -tradeFeedVolumeFilter) {
        var updown;
        var tradeupdown;
        tradeupdownicon = "<i class='fa fa-chevron-up fa-blank'></i>"
        if(amount > tradeFeedVolumeFilter){
            updown = "up"
            tradeupdown = "<span class='trades-positive'></span>"

        }else if(amount < -tradeFeedVolumeFilter){
            updown = "down"
            tradeupdown = "<span class='trades-negative'></span>"
            $('.trades-negative').css('opacity', amount);
        }
        if(price > lastprice){
            tradeupdownicon = "<i class='fas fa-chevron-up'></i>"
        }else if(price < lastprice){
            tradeupdownicon = "<i class='fas fa-chevron-down'></i>"

        }

        var contentLength = $(".trades-btc > div").length;
        var contentstyle = "<div class='clearfix'></div>"
        var contenttime = "<div class='trade-col1'>"+tradeupdownicon +"</div>"
        var contentprice = "<div class='trade-col2'>"+price+"</div>"
        var contentamount = "<div class='trade-col3'>"+amount+"</div>"
        var tradeitem = "<div class='trade-item'>"+tradeupdown+contenttime + contentprice + contentamount+contentstyle+"</div>"
        $(".trades-btc").prepend(tradeitem);
        var lasttradeslength = $('#tradeFeedLengthFilter').val();
        if(contentLength > lasttradeslength){
            var $ul = $(".trades-btc")
            $ul.find('div').slice(-lasttradeslength).remove()
        }
        lastprice = price;


    }

}


function ltcusdtrades(time, price, amount){
    var tradeFeedVolumeFilter = $('#tradeFeedVolumeFilter').val();
    if (amount > tradeFeedVolumeFilter || amount < -tradeFeedVolumeFilter) {
        var contentLength = $(".trades-ltc > div").length;
        var contenttime = "<div class='trade-col1'>up</div>"
        var contentprice = "<div class='trade-col2'>"+price+"</div>"
        var contentamount = "<div class='trade-col3'>"+amount+"</div>"
        var tradeitem = "<div class='trade-item'>"+contenttime + contentprice + contentamount+"</div>"
        $(".trades-ltc").prepend(tradeitem);
        var lasttradeslength = $('#tradeFeedLengthFilter').val();
        if(contentLength > lasttradeslength){
            var $ul = $(".trades-ltc")
            $ul.find('div').slice(0, lasttradeslength).remove()
        }
    }
}


/* LIST METHOD OLD
function ltcusdtrades(time, price, amount){
    var tradeFeedVolumeFilter = $('#tradeFeedVolumeFilter').val();
    if (amount > tradeFeedVolumeFilter || amount < -tradeFeedVolumeFilter) {
        var contentLength = $('ul#ltcusd > li').length
        var content = "<li>" + "time:"+time + " price:" + price + " amount:" + amount + "</li>";
        var lasttradeslength = $('#tradeFeedLengthFilter').val();
        $('ul#ltcusd').prepend(content);
        if(contentLength > lasttradeslength){
            var $ul = $('#ltcusd')
            $ul.find('li').slice(lasttradeslength).remove()
        }
    }
}
 */






function getticker(response){
   var bid = response[1]
    var bid_size = response[2]
    var ask = response[3]
    var daily_change = response[5]
    var lastChangePcnt = response[6]
    var  price = response[7]
    var    volume = response[8]
}

/*
class=btc, btc_price, lastChangePcnt
[
    SYMBOL,
    BID,
    BID_SIZE,
    ASK,
    ASK_SIZE,
    DAILY_CHANGE,
    DAILY_CHANGE_PERC,
    LAST_PRICE,
    VOLUME,
    HIGH,
    LOW
],
 */
