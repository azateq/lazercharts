
var ws4 = new WebSocket('wss://api.bitfinex.com/ws/');


// updates
/*
   "<CHANNEL_ID>",0
   "<BID>",1
   "<BID_SIZE>",2
   "<ASK>",3
   "<ASK_SIZE>",4
   "<DAILY_CHANGE>",5
   "<DAILY_CHANGE_PERC>",6
   "<LAST_PRICE>",7
   "<VOLUME>",8
   "<HIGH>",9
   "<LOW>"10
 */


var id4 = "bitfinex_ticker"
var buf4 = {};
buf4['bitfinex_ticker'] = [[], []];
var price;
var currency = "$";
var chan_btcusd;
var chan_ltcusd;
var chan_ethusd;
var response;



ws4.onopen = function() {

    ws4.send(JSON.stringify({ event: 'subscribe', channel: 'ticker', pair: 'BTCUSD'}))
    ws4.send(JSON.stringify({ event: 'subscribe', channel: 'ticker', pair: 'LTCUSD'}))
    ws4.send(JSON.stringify({ event: 'subscribe', channel: 'ticker', pair: 'ETHUSD'}))
};
// print out quotes every 1000 ms (1 second)
        ws4.onmessage = function(msg) {     // callback on message receipt
            response = JSON.parse(msg.data);


            if (response["pair"] == "BTCUSD") {
                chan_btcusd = response["chanId"]
                btcpair = response["pair"]
            }
            if (response["pair"] == "LTCUSD") {
                chan_ltcusd = response["chanId"]
                ltcpair = response["pair"]
            }
            if (response["pair"] == "ETHUSD") {
                chan_ethusd = response["chanId"]
                ethpair = response["pair"]
            }
            if (response[0] == chan_btcusd) {
                bid = response[1]
                bid_size = response[2]
                ask = response[3]
                daily_change = response[5]
                lastChangePcnt = response[6]
                price = response[7]
                volume = response[8]
                if (response[7] > 0) {    // Only 'te' message type is needed
                    btcusdprice();
                }
            }
            if (response[0] == chan_ltcusd) {
                bid = response[1]
                bid_size = response[2]
                ask = response[3]
                lastChangePcnt = response[6]
                price = response[7]
                volume = response[8]
                if (response[7] > 0) {
                    ltcusdprice();
                }// Only 'te' message type is needed
            }
            if (response[0] == chan_ethusd) {
                bid = response[1]
                bid_size = response[2]
                ask = response[3]
                lastChangePcnt = response[6]
                price = response[7]
                volume = response[8]
                if (response[7] > 0) {
                    ethusdprice();
                }// Only 'te' message type is needed
            }
        }

            //   updateData(myChart, [response[3]], 1)


/*
/*
$.ajax({
    url: "https://api.bitfinex.com/v2/tickers?symbols=tBTCUSD",
    type: 'GET',
    crossDomain: true,
}).done(function(data) {
    console.log(data)
    // request = JSON.stringify(data);
    console.log(data[0][0]);
    $('.btc .name').append(data[0][0])
    $('.btc .price').append(data[0][7])
});

 */



/*


function addData(chart, data, datasetIndex) {
    chart.data.datasets[datasetIndex].data = data;
    chart.update();
}

var gradientStroke = 'rgba(0, 119, 220, 0.6)',
    gradientFill = 'rgba(0, 119, 220, 0.4)';

var ctx = document.getElementById(id4).getContext("2d");

var chart = new Chart(ctx, {
    type: 'line',

    data: {

        datasets: [{

            data: [],
        },{

            data: [],
        }]

    },

    options: {
        title: {
            text: 'BTC/USD (' + id4 + ')', // chart title
            display: true
        },
        scales: {

            xAxes: [{
                type: 'realtime'
            }],
            yAxes: [{
                position: 'right',
            }]
        },
        plugins: {

            streaming: {
                duration: 15000,
                onRefresh: function(chart) {
                    chart.data.datasets[0].data.push({
                        x: Date.now(),
                        y: price
                    }),
                        chart.data.datasets[1].data.push({
                            x: Date.now(),

                            y: ask
                        })

                    amountneg = 0;
                    amountpos = 0;
                },
                delay: 2000

            }

        }

    }})
/*
/*
setTimeout(function() {
    addData(chart, response, 0);
}, 2000);



function addData(chart, data, datasetIndex) {
    chart.data.datasets[datasetIndex].data = data;
    chart.update();
}
function updateData(chart, data) {
    chart.data.datasets[0].data = data,
        chart.update();

}
function updateLabel(chart, label) {
    chart.data.labels = label,
        chart.update();
}
function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}

*/