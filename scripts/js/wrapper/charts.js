
function createChart(pair) {
    new window.TradingView.widget({
        "container_id": "tv",
        "autosize": true,
        "symbol": pair,
        "interval": "15",
        "timezone": "Europe/Berlin",
        "theme": "Dark",
        "style": "1",
        "locale": "en",
        "toolbar_bg": "rgba(19, 23, 35, 1)",
        "hide_top_toolbar": false,
        "left_toolbar": true,
        "hide_side_toolbar": true,
        "allow_symbol_change": true,
        "hideideas": true,
        "show_popup_button": true,
        "studies": [
            "BB@tv-basicstudies",
            "MASimple@tv-basicstudies",
        ],

        "studies_overrides": {
            "bollinger bands.median.color": "rgba(255, 0, 0, 0)",
            "bollinger bands.upper.linewidth": 217
        },
        disabled_features: ["use_localstorage_for_settings"],
        debug: true,
        user_id: 'pride',
        overrides: {
            "symbolWatermarkProperties.color": "rgba(255, 0, 0, 0)",
            "paneProperties.background": "#131723",
            "paneProperties.gridProperties.color": "#E6E6E6",
            "paneProperties.vertGridProperties.color": "E6E6E6",
            "paneProperties.horzGridProperties.color": "E6E6E6",
            "paneProperties.crossHairProperties.color": "B7B7B7",
            "scalesProperties.backgroundColor": "rgba(109, 68, 157, 0.8)",


        }
    });

}



Chart.defaults.global.animationSteps = 60;

var id = 'Bitfinex';
var ctx = document.getElementById(id).getContext('2d');
var chart = new Chart(ctx, {
    type: 'bubble',
    data: {
        datasets: [{
            data: [],
            label: 'buyamount',
            type: "bubble",
            spanGaps: true,
            cubicInterpolationMode: 'linear',
            borderColor: '#52b987',
            backgroundColor: '#52b987',
            fill: true,
            lineTension:1,                    // straight line
        }, {
            data: [],
            label: 'sellamount',
            type: "bubble",
            backgroundColor:'#eb4d5c', // fill color
            spanGaps: true,
            borderColor: '#eb4d5c',
            cubicInterpolationMode: 'linear',
            fill: true,
            lineTension: 1,                           // no fill
        }]
    },
    options: {
        animation: {
            duration: 0
        },
        hover: {
            animationDuration: 0, // duration of animations when hovering an item
        },
        responsiveAnimationDuration: 0, // animation duration after a resize
        title: {
            text: '(' + id + ')', // chart title
            display: true
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    fontColor: "#cecece", // this here
                },
                gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    zeroLineColor: '#363c4ea3'
                },
                type: 'realtime' // auto-scroll on X axis
            }],
            yAxes: [{
                ticks: {
                    fontColor: "#cecece", // this here
                },
                gridLines: {
                    color: "#363c4e3b",
                    zeroLineColor: '#363c4ea3'
                },
                labelString: "Amount",
                position: 'right'

            }]
        },
        responsive: true,
        plugins: {
            streaming: {
                duration: 50000, // display data for the latest 300000ms (5 mins)
                delay:0,
                onRefresh: function(chart) { // callback on chart update interval
                    /*
                      chart.data.datasets[0].data.push({ // THIS WORKS
                        x: Date.now(),
                        y: Math.random() * 100
                    });
chart.data.datasets[2].data.push({ // THIS WORKS
                        x: Date.now(),
                        y: Math.random() * 105
                    });
                     */



                    Array.prototype.push.apply(  // THIS NOT TODO
                        chart.data.datasets[0].data, buf[id][0]
                    );            // add 'buy' price data to chart
                    Array.prototype.push.apply(
                        chart.data.datasets[1].data, buf[id][1]
                    );

                    // add 'sell' price data to chart
                    buf[id] = [[], []]; // clear buffer
                }
            }
        }
    }
});
