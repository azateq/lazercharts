var ws = new WebSocket('wss://api.bitfinex.com/ws/');
var orderbookchanId;
var tickerchanId;
var tradechanId;
var snap = true;


var BTCUSD_tickerpair = "BTCUSD"
var LTCUSD_tickerpair = "LTCUSD"
var ETHUSD_tickerpair = "ETHUSD"
var EOSUSD_tickerpair = "EOSUSD"
var XMRUSD_tickerpair = "XMRUSD"
var ZECUSD_tickerpair = "ZECUSD"
var NEOUSD_tickerpair = "NEOUSD"

var arrTickerpair = []
arrTickerpair.push(
    BTCUSD_tickerpair,
    LTCUSD_tickerpair,
    ETHUSD_tickerpair,
    EOSUSD_tickerpair,
    XMRUSD_tickerpair,
    ZECUSD_tickerpair,
    NEOUSD_tickerpair
)

var BTCUSD_tickerchan = null;
var LTCUSD_tickerchan = null;
var ETHUSD_tickerchan = null;
var EOSUSD_tickerchan = null;
var XMRUSD_tickerchan = null;
var ZECUSD_tickerchan = null;
var NEOUSD_tickerchan = null;


var TICKERMAP =  {}

var data;
var response;
var chanId;

var buf = {};
buf['Bitfinex'] = [[], []];

function orderchannel(pair) {
    var price = 0;

    ws.onopen = function () {
        //ORDERBOOK
        ws.send(JSON.stringify({      // send subscribe request
            "event": "subscribe",
            "channel": "book",
            "pair": pair,
            "prec": "P2",
            "len": "25",
            "freq": "F0"
        }));
        snap = true;
        book = true;
        console.log("Connected to WebSocket ws:")
        console.log(ws)


        //TRADES
        ws.send(JSON.stringify({      // send subscribe request
            "event": "subscribe",
            "channel": "trades",
            "pair": pair
        }));

        //Tickersubs
        arrTickerpair.forEach(function(tickerpair) {
            ws.send(JSON.stringify({event: 'subscribe', channel: 'ticker', pair: tickerpair}))
        });
    };


    ws.onmessage = function (msg) {     // callback on message receipt
        data = msg.data
        response = JSON.parse(msg.data);
        chanId = response[0];
        if (response["event"] == "subscribed") {
            if (response["channel"] == "book") {
                orderbookchanId = response["chanId"]
                pair = response["pair"]
                console.log(response["event"])
            }
            if (response["channel"] == "ticker") {
                TICKERMAP[response["pair"]] = {chanId: response["chanId"]}
            }

            if (response["channel"] == "trades") {
                tradechanId = response["chanId"]
                pair = response["pair"]
                console.log(response["event"])
            }
        }

        //Call Tickers for Widgets in js/wrapper/ticker.js
        //tickerfunc();
        tickerfunc(chanId);

        if (tradechanId == chanId) {
            if (response[1] === 'te') {    // Only 'te' message type is needed
                timestamp = response[3]
                var timestampInMilliSeconds = timestamp * 1000;
                var time = new Date(timestampInMilliSeconds);

                price = response[4]
                amount = response[5]

                trades(timestamp, price, amount, pair);


                /*

                 buf['Bitfinex'][response[5] > 0 ? 0 : 1].push({
                     x: response[3] * 1000, // timestamp in milliseconds
                     y: response[5],   // amountr
                     r: tempvar * 5
                 });
                  */
                tempvar = response[5] // Bubbleradius = Coinamount * 5
                if (tempvar < 0) {
                    tempvar = response[5] * -1;
                    tempvar = tempvar
                }
                if (tempvar > 3) { //maxsize
                    tempvar = 3;
                }
                var tradeFeedVolumeFilter = $('#tradeFeedVolumeFilter').val();
                // Only Show Bubbles if over mintradeamounttoshowfilter
                if (amount > tradeFeedVolumeFilter || amount < -tradeFeedVolumeFilter) {
                    buf['Bitfinex'][response[5] > 0 ? 0 : 1].push({
                        // x: response[3] * 1000, // timestamp in milliseconds
                        x: Date.now(), // timestamp in milliseconds
                        y: response[5],     // amount
                        r: tempvar * 1.5
                    });
                }


            }
        }


        if (orderbookchanId == chanId) {
            if (snap) {
                highestcoinamount = 0;
                for (i = 0, cnt = response[1].length; i < cnt; i++) {
                    pricelevel = response[1][i][0]
                    orderamount = response[1][i][1]
                    coinamount = response[1][i][2]
                    orderbooksnapshot(pricelevel, orderamount, coinamount);
                }
                //after snap scrollto middle of book
                $('#complete').mCustomScrollbar('scrollTo','50%');
                snap = false;
            }
            if (!snap) {
                priceupdate = response[1]
                count = response[2]
                coinamountupdate = response[3]
                orderbookupdate(priceupdate, count, coinamountupdate);
            }
        }


    }
}

ws.onclose = function (event) {
    snap = true;

    console.log("Disconnected from WebSocket")
};


