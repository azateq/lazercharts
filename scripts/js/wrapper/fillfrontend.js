var lastprice = 0;
var tickerpricestring = 0;


function fillliquidations(symbol, longshort, contract, liqprice) {
    "use strict";
    var current_time = new moment().format("HH:mm:ss");


    var liqstring = "[" + current_time + "] " + symbol + " " + longshort + " > liquidated! " + contract + " contracts at $" + liqprice + "!"



    if(longshort == "short")
        var liqitem ="<TR class='liqrow positive'>" +
            "<TD class='liqtime'>" +"[" + current_time + "] " + "</TD>" +
            "<TD class='liqsymbol'>" + symbol + " " + "</TD>" +
            "<TD class='longshort'>" + longshort + "</TD>" +
            "<TD>" + "> liquidated! " + "</TD>" +
            "<TD class='contract'>" + contract +"</TD>" +
            "<TD class='liqprice'>" + "contracts at " + "</TD>" +
            "<TD class='liqprice'>" + "$"+ liqprice + "</TD>" +
            "</TR>";
    if(longshort == "long")
        var liqitem ="<TR class='liqrow negative'>" +
            "<TD class='liqtime'>" +"[" + current_time + "] " + "</TD>" +
            "<TD class='liqsymbol'>" + symbol + " " + "</TD>" +
            "<TD class='longshort'>" + longshort + "</TD>" +
            "<TD>" + "> liquidated! " + "</TD>" +
            "<TD class='contract'>" + contract +"</TD>" +
            "<TD class='liqprice'>" + "contracts at " + "</TD>" +
            "<TD class='liqprice'>" + "$"+ liqprice + "</TD>" +
            "</TR>";

    $(".liquidationstable > tbody:last-child").prepend(liqitem)

}

function fillchat(chatmsg, user) {
    "use strict";
    var current_time = new moment().format("HH:mm:ss");

    var chatitem ="<TR class='chatrow'>" +
        "<TD class='chattime'>" + user + "</TD>" +
        "<TD class='chatmsg'>" + chatmsg + "</TD>" +
        "</TR>";


    $(".chattable > tbody").prepend(
        $(chatitem).hide().fadeTo('fast', 1)
);

    //$('.chat').mCustomScrollbar('scrollTo','last');

    //max chatitems ++ wenn chatlength größer x dann remove first two chateinträge
    var maxchatitem = 25;
    /* append
    if( $(".chattable tr").length > maxchatitem){
        $(".chattable tr").slice(0, 1).remove();
    }
     */


    $(".chattable").find('tr').slice(maxchatitem).remove()


}

function fillheatmap(tickerpair, tickerprice, currency, DAILY_CHANGE_PERC, daily_change, volume, high, low) {
    "use strict";
    DAILY_CHANGE_PERC = DAILY_CHANGE_PERC * 100;
    tickerpricestring = currency + " " + tickerprice.toFixed(2) // price + currency string
    $('.' + tickerpair).find('.price').text(tickerpricestring); // setprice
    var lastprice = $('.' + tickerpair).find('.price').val() // lastprice
    $('.' + tickerpair).find('.pair').text(tickerpair); // PAIR
    $('.' + tickerpair).find('.DAILY_CHANGE_PERC').text(DAILY_CHANGE_PERC.toFixed(2) + "%"); //DAILYCHANGE

    if (volume > 0)
        $('.' + tickerpair).find('.VOLUME').text(volume.toFixed(0)); //dailyvolume

    $('.' + tickerpair).find('.daily_change').text(daily_change.toFixed(2)); //dailyvolume
    $('.' + tickerpair).find('.HIGH').text(high.toFixed(2)); //dailyvolume
    $('.' + tickerpair).find('.LOW').text(low.toFixed(2)); //dailyvolume
    //+ " " + tickerpair.substring(0,3)


    if (tickerprice > lastprice) {
        $('.' + tickerpair).find('.price').addClass('positive');
        setTimeout(function () {
            $('.' + tickerpair).find('.price').removeClass('positive');
        }, 2000);

    } else {
        $('.' + tickerpair).find('.price').addClass('negative');
        setTimeout(function () {
            $('.' + tickerpair).find('.price').removeClass('negative');
        }, 2000);
    }
    if (DAILY_CHANGE_PERC > 0.00) {
        $('.' + tickerpair).find('.DAILY_CHANGE_PERC').addClass('positive').removeClass('negative')
        $('.' + tickerpair).find('.DAILY_CHANGE').addClass('positive').removeClass('negative')
    } else {
        $('.' + tickerpair).find('.DAILY_CHANGE_PERC').addClass('negative').removeClass('positive')
        $('.' + tickerpair).find('.DAILY_CHANGE').addClass('negative').removeClass('positive')
    }
}

var highestcoinamount = 0;
var multiplier;
var maxamount;


function orderbooksnapshot(pricelevel, orderamount, coinamount) {
    "use strict";
    if (orderamount > 0) {
        if (coinamount < 0) {
            var coinamountpos = coinamount * -1;
            if (highestcoinamount < coinamountpos) {
                highestcoinamount = coinamountpos / 2
            }
            var coinbar = "<span class='coinbar coinbarsell'></span>"
            var clearfix = "<div class='div-table-col clearfix'></div>"
            var pricelevel = "<div class='div-table-col pricelevel order-col2'>" + pricelevel + "</div>"
            var orderamount = "<div class='div-table-col orderamount order-col3'>" + orderamount + "</div>"
            var coinamountdiv = "<div class='div-table-col coinamount coinbarsellcolor order-col1'>" + coinamountpos.toFixed(2) + "</div>"
            var orderitem = "<div class='order-item'>" + coinbar + coinamountdiv + pricelevel + orderamount + clearfix + "</div>"
            var row = "<div class='div-table-row'>" + orderitem + "</div>";

            $(".asks").after(row)
        }
        else if (coinamount > 0) {
            if (highestcoinamount < coinamount) {
                highestcoinamount = coinamount / 2
            }

            var coinbar = "<span class='coinbar coinbarbid'></span>"
            var clearfix = "<div class='clearfix'></div>"
            var pricelevel = "<div class='div-table-col pricelevel order-col2'>" + pricelevel + "</div>"
            var orderamount = "<div class='div-table-col orderamount order-col3'>" + orderamount + "</div>"
            var coinamountdiv = "<div class='div-table-col coinamount coinbarbidcolor order-col1'>" + coinamount.toFixed(2) + "</div>"
            var orderitem = "<div class='order-item'>" + coinbar + coinamountdiv + pricelevel + orderamount + clearfix + "</div>"
            var row = "<div class='div-table-row'>" + orderitem + "</div>";

            $(".bids").before(row)
        }
    }
}


var barcalc = function (pair) {
    "use strict";
    switch (pair) {
        case 'BTCUSD':
            multiplier = 190;
            maxamount = 350;
            break;
        case 'LTCUSD':
            multiplier = 100;
            maxamount = 5000;
            break;
        case 'ETHUSD':
            multiplier = 90;
            break;
        case 'EOSUSD':
            multiplier = 400;
            break;
        case 'XMRUSD':
            multiplier = 30;
            break;
        default:
            multiplier = 400;
    }
    return multiplier;
}

function orderbookupdate(priceupdate, count, coinamountupdate) {
    "use strict";
    /* TODO
    https://github.com/mhuggins/jquery-countTo
https://jqueryui.com/draggable/#constrain-movement
parallex
     */

    /*
    check every order-item
     */
    $('#complete .order-item').each(function () {
            if (count > 0) { // update order-item  (< 0 means delete this order-item)
                if (coinamountupdate < 0) { // ask side

                    var coinamountask = $(this).children('.coinamount').text() // save value coinamount each order-item

                    /* coinbarwith
                    calc + animate coinbarwith (orderbook transparentbar for coinamount each pricelvl
                     */
                    barcalc(pair); //calc barwidth
                    if (highestcoinamount > maxamount) { // use maxamount if highestcoinamount is to high -- else barwidth problems
                        highestcoinamount = maxamount;
                        console.log("maxamount " + maxamount)
                    }
                    var barwidth = coinamountask / highestcoinamount * multiplier

                    // set barwidth anyways (update or not)
                    $(this).children('.coinbar').css('width', barwidth);

                    /* coinbarwith
                    laggy bar animation
                    $(this).children('.coinbar').css('width', barwidth).animate({}, 4500);
                    */

                    // save pricelevel for each order-item
                    var pricelevel = $(this).children('.pricelevel').text()

                    // check if parameter(priceupdate) == current pricelevel in this for each
                    if (priceupdate == pricelevel) {



                        // make update positive since we are on askside
                        var coinamountupdatepos = coinamountupdate.toFixed(2) * -1;



                        //fade in
                        $(this).fadeIn("slow", function () {
                            "use strict";
                        });


                        if(coinamountask > coinamountupdatepos){
                            $(this).addClass("negative").delay(100).queue(function (next) {
                                $(this).removeClass("negative");
                                next();
                            });
                        }else{
                            $(this).addClass("positive").delay(100).queue(function (next) {
                                $(this).removeClass("positive");
                                next();
                            });
                        }


                        $(this).children('.orderamount').text(count);
                        $(this).children('.coinamount').text(coinamountupdatepos);
                        // update barwidth ++ add classes for sellside (color + backgroundcolor)
                        $(this).children('.coinbar').css('width', barwidth);
                        $(this).children('.coinbar').removeClass('coinbarbid').addClass('coinbarsell')
                        $(this).children('.coinamount').addClass('coinbarsellcolor').removeClass("coinbarbidcolor");




                    }

                }
                if (coinamountupdate > 0) {
                    var coinamountbid = $(this).children('.coinamount').text()

                    barcalc(pair); //calc barwidth
                    if (highestcoinamount > maxamount) {
                        highestcoinamount = maxamount;
                        console.log("maxamount " + maxamount)
                    }
                    var barwidth = coinamountbid / highestcoinamount * multiplier

                    $(this).children('.coinbar').css('width', barwidth);

                    var pricelevel = $(this).children('.pricelevel').text()
                    if (priceupdate == pricelevel) {

                        $(this).fadeIn("slow", function () {
                            "use strict";
                        });

                        if(coinamountbid > coinamountupdate){
                            $(this).addClass("negative").delay(100).queue(function (next) {
                                $(this).removeClass("negative");
                                next();
                            });
                        }else{
                            $(this).addClass("positive").delay(100).queue(function (next) {
                                $(this).removeClass("positive");
                                next();
                            });
                        }

                        $(this).children('.orderamount').text(count);
                        $(this).children('.coinamount').text(coinamountupdate.toFixed(2));
                        $(this).children('.coinbar').css('width', barwidth)
                        $(this).children('.coinbar').removeClass('coinbarsell').addClass('coinbarbid')
                        $(this).children('.coinamount').addClass('coinbarbidcolor').removeClass('coinbarsellcolor');



                    }
                }
            }

            if (count == 0) {
                if (coinamountupdate == -1) {
                    var pricelevel = $(this).children('.pricelevel').text()
                    if (priceupdate == pricelevel) {
                        $(this).fadeOut("slow", function () {
                            "use strict";
                        })
                    }
                }
            }
        }
    );


    /*
     $('#asks .order-item').each(function () {
            if (count > 0) {
                    if (coinamountupdate < 0) {

                        var coinamountask = $(this).children('.coinamount').text()
                        if(highestcoinamount > 100){
                            highestcoinamount = 100;
                        }
                        barwidth = coinamountask/highestcoinamount * highestcoinamount

                        $(this).children('.coinbar').css('width', barwidth).animate({
                        }, 2500);


                        var pricelevel = $(this).children('.pricelevel').text()

                        if (priceupdate == pricelevel) {
                            found = true;
                           var coinamountupdatepos = coinamountupdate.toFixed(2)*-1
                            $(this).fadeIn( "slow", function(){
                                "use strict";
                            });
                            $(this).children('.coinbar').css('width', barwidth)
                            $(this).children('.orderamount').text(count);
                            $(this).children('.coinamount').text(coinamountupdatepos);
                             Highlight COLUMN
                            $(this).addClass("updatered").delay(1000).queue(function (next) {
                                $(this).removeClass("updatered");c
                                next();
                            });



        Highlight ROW





    }
    }
    }
    if (count == 0){
        if (coinamountupdate == -1) {
            var pricelevel = $(this).children('.pricelevel').text()
            if (priceupdate == pricelevel) {
                $(this).fadeOut( "slow", function(){
                    "use strict";

                })


            }
        }
    }
    }
    );
     */

    /*
     $('#bids .order-item').each(function () {
            if (count > 0) {
                if (coinamountupdate > 0) {
                    var coinamountbid = $(this).children('.coinamount').text()
                    if(highestcoinamount > 300){
                        highestcoinamount = 300;
                    }
                    barwidth = coinamountbid/100 * highestcoinamount
                    $(this).children('.coinbar').css('width', barwidth).animate({
                    }, 2500);$

                    var pricelevel = $(this).children('.pricelevel').text()
                    if (priceupdate == pricelevel) {
                        $(this).fadeIn( "slow", function(){
                            "use strict";
                        });
                        found = true;
                        $(this).children('.orderamount').text(count);
                        $(this).children('.coinamount').text(coinamountupdate.toFixed(2));
                        /*
                         $(this).children('.coinamount').addClass("updatecell").delay(500).queue(function (next) {
                                                $(this).removeClass("updatecell");
                                                next();
                                            });

    }
    }
    }
    if (count == 0){
        if (coinamountupdate == 1) {
            var pricelevel = $(this).children('.pricelevel').text()
            if (priceupdate == pricelevel) {



                $(this).fadeOut( "slow", function(){
                    "use strict";

                })

            }

        }
    }
    });
     */


    /*
    OrderbookIFnotFOUND add


    if(found == false){
        if (count > 0) {
            if (coinamountupdate < 0) {
                var coinbar = "<span class='coinbar'></span>"

                var clearfix = "<div class='clearfix'></div>"
                var pricelevel = "<div class='pricelevel order-col2'>" + priceupdate + "</div>"

                var orderamount = "<div class='orderamount order-col3'>" + count + "</div>"

                var coinamountdiv = "<div class='coinamount  order-col1'>" + coinamountupdate.toFixed(2) + "</div>"
                var orderitem = "<div class='order-item'>" + coinbar + coinamountdiv + pricelevel + orderamount + clearfix + "</div>"

                $("#complete").prepend(orderitem)
                $("#asks").prepend(orderitem)

                console.log("prepended")
                var coinamountupdatepos = coinamountupdate.toFixed(2);

                $(this).fadeIn("slow", function () {
                    "use strict";
                });

                $(this).children('.coinbar').css('width', barwidth)
                $(this).children('.coinamount').css('color', 'red')
                $(this).children('.orderamount').text(count);
                $(this).children('.coinamount').text(coinamountupdatepos);


            }
        }
        if (count < 0) {
            if (coinamountupdate > 0) {
                var coinbar = "<span class='coinbar'></span>"
                var clearfix = "<div class='clearfix'></div>"
                var pricelevel = "<div class='pricelevel order-col2'>" + priceupdate + "</div>"

                var orderamount = "<div class='orderamount order-col3'>" + count + "</div>"


                var coinamountdiv = "<div class='coinamount  order-col1'>" + coinamountupdate.toFixed(2) + "</div>"
                var orderitem = "<div class='order-item'>" + coinbar + coinamountdiv + pricelevel + orderamount + clearfix + "</div>"

                $("#complete").append(orderitem)
                $("#bids").append(orderitem)
                console.log("append")

            }
        }
        if (count == 0){
            if (coinamountupdate == -1) {
                var pricelevel = $(this).children('.pricelevel').text()
                if (priceupdate == pricelevel) {
                    $(this).fadeOut( "slow", function(){
                        "use strict";

                    })


                }
            }
        }

    }
*/
    /*
    timestamp update for trades
     */


}

function trades(timestamp, price, amount) {
    var tradeupdownicon = "<i class='fa fa-chevron-up fa-blank'></i>"
    var updown;
    var tradeupdown;
    price = price.toFixed(2);
    amount = amount.toFixed(5);
    var contentstyle = "<span class=''></span>";
    var tradeFeedVolumeFilter = $('#tradeFeedVolumeFilter').val();
    var tradeFeedVolumeHighlight = $('#tradeFeedVolumeHighlight').val();
    var tradeFeedAlarm = $('#tradeFeedAlarm').val();

    var dateString = moment.unix(timestamp).format("h:mm:ss");

    var contentLength = $(".trades > div").length;
    var lasttradeslength = $('#tradeFeedLengthFilter').val();

    if (contentLength > lasttradeslength) {
        var $ul = $(".trades")
        $ul.find('.div-table-row').slice(lasttradeslength - 1).remove()
    }

    if (price > lastprice) {
        tradeupdownicon = "<i class='fas fa-chevron-up'></i>"
    } else if (price < lastprice) {
        tradeupdownicon = "<i class='fas fa-chevron-down'></i>"
    }

    if (amount > tradeFeedVolumeFilter || amount < -tradeFeedVolumeFilter) {
        var i = 0;
        if (amount > 0) {
            var plusminus = '+'
        } else {
            var plusminus = ''
        }
        var clearfix = "<div class='clearfix'></div>"
        var contentupdown = "<div class='div-table-col trade-col1'>" + tradeupdownicon + "</div>"
        var timestamp = "<div class='div-table-col timestamp'>" + dateString + "</div>"
        var contentprice = "<div class='div-table-col trade-col2'>" + price + "</div>"
        var contentamount = "<div class='div-table-col trade-col3'>" + plusminus + amount + "</div>"


        var tradeitem = "<div class='trade-item'>" + contentupdown + timestamp + contentprice + contentamount + clearfix + "</div>"
        var row = "<div class='div-table-row'>" + tradeitem + "</div>";

        $('.trades').prepend(
            $(row).hide().fadeIn('fast')
        );


        $('#users tbody > tr').each(function () { // checkt alle pricealarms
            "use strict";

            if (pair.toLowerCase() === $(this).children('tr > .alarmpair').text().toLowerCase()) {
                var pricalarm = $(this).children().text(); // speichere jeden durchlauf in pricealarm
                if (price > pricalarm) { // wenn preis über alarm do shit
                    $(this).children().addClass("alarmbelowprice") // remove alarm
                } else if (price < pricalarm) { // wenn preis über alarm do shit
                    $(this).children().addClass("alarmoverprice") // remove alarm
                }
            }
        })


        if (amount > tradeFeedVolumeHighlight || amount < -tradeFeedVolumeHighlight) {
            $('.div-table-row:first-child .trade-col3').css('text-decoration', 'underline');
            //$('.div-table-row:first-child .trade-col3').css('font-weight', 'bold');
            /*
              if (amount > tradeFeedVolumeHighlight) {

                            $('.div-table-row:first-child').css('background', 'rgba(0, 113, 0,' + 0.3 + ')');

                        }else if (amount < -tradeFeedVolumeHighlight) {
                            x = amount * -1
             var transparenz = x;
                            if(transparenz > 0.4)
                                transparenz = 0.4



                        $('.div-table-row:first-child').css('background', 'rgba(255, 0, 2,' + 0.3 + ')');

                    }
             */
        }





        if (amount > tradeFeedVolumeFilter) {

            $('.div-table-row:first-child .trade-col3').css('color', '#52b987');
            $('.div-table-row:first-child .trade-col1').css('color', '#52b987');
        } else if (amount < -tradeFeedVolumeFilter) {
            amount = amount * -1

            /*
             $('.trade-item:first-child').css('background', 'rgba(255, 0, 2, 0.4)');
              */

            $('.div-table-row:first-child .trade-col1').css('color', '#eb4d5c');
            $('.div-table-row:first-child .trade-col3').css('color', '#eb4d5c');
        }

        "use strict";
        if ($("#showtime").is(':checked'))
            $(".timestamp").css('color', '#5a6569').fadeIn();  // checked
        else
            $(".timestamp").css('color', 'transparent');  // unchecked


        lastprice = price;
        i = i + 1;

    }


}

/*
function orders(price, count, amount) {
    var tradeupdownicon = "<i class='fa fa-chevron-up fa-blank'></i>"
    var updown;
    var tradeupdown;
    price = price.toFixed(2);
    amount = amount.toFixed(5);
    var contentstyle = "<span class=''></span>";
    var tradeFeedVolumeFilter = $('#tradeFeedVolumeFilter').val();


    if (price > lastprice) {
        tradeupdownicon = "<i class='fas fa-chevron-up'></i>"
    } else if (price < lastprice) {
        tradeupdownicon = "<i class='fas fa-chevron-down'></i>"

    }
    if (amount > tradeFeedVolumeFilter || amount < -tradeFeedVolumeFilter) {
        var i = 0;
        var contentLength = $(".trades > div").length;

        var clearfix = "<div class='clearfix'></div>"
        var contentupdown = "<div class='trade-col1'>" + tradeupdownicon + "</div>"
        var contentprice = "<div class='trade-col2'>" + price + "</div>"
        var contentamount = "<div class='trade-col3'>" + amount + "</div>"


        var tradeitem = "<div class='trade-item'>" + contentstyle + contentupdown + contentprice + contentamount + clearfix + "</div>"
        $(".trades").prepend(tradeitem)

        if (amount > tradeFeedVolumeFilter) {
            $('.trade-item:first-child').css('background-color', 'rgba(0, 128, 0,' + amount);
        } else if (amount < -tradeFeedVolumeFilter) {
            amount = amount * -1
            $('.trade-item:first-child').css('background-color', 'rgba(255, 0, 2,' + amount);
        }

        var lasttradeslength = $('#tradeFeedLengthFilter').val();
        if (contentLength > lasttradeslength) {
            var $ul = $(".trades")
            $ul.find('div').slice(-lasttradeslength).remove()
        }
        lastprice = price;
        i = i + 1;

    }

}

 */


/*
class=btc, btc_price, lastChangePcnt
[
    SYMBOL,
    BID,
    BID_SIZE,
    ASK,
    ASK_SIZE,
    DAILY_CHANGE,
    DAILY_CHANGE_PERC,
    LAST_PRICE,
    VOLUME,
    HIGH,
    LOW
],
 */
