
function orderchannel(pair) {
    var tradechan;
    var ws = new WebSocket('wss://api.bitfinex.com/ws2/');
    var price = 0;
    var snapshot;
    var snap = true;
    var update = false;
    var counter = 0;
    const BOOK = {}




    ws.onopen = function () {
        "use strict";
        BOOK.bids = {}
        BOOK.asks = {}
        BOOK.psnap = {}
        BOOK.mcnt = 0
        ws.send(JSON.stringify({      // send subscribe request
            "event": "subscribe",
            "channel": "book",
            "pair": pair,
            "prec": "P2",
            "len": "25",
            "freq": "F0"
        }));
    };


    ws.onmessage = function (msg) {     // callback on message receipt
        msg = JSON.parse(msg)
        if (msg.event) return
        if (msg[1] === 'hb') return

        // if msg contains checksum, perform checksum
        if (msg[1] === 'cs') {
            const checksum = msg[2]
            const csdata = []
            const bidsKeys = BOOK.psnap['bids']
            const asksKeys = BOOK.psnap['asks']

            // collect all bids and asks into an array
            for (let i = 0; i < 25; i++) {
                if (bidsKeys[i]) {
                    const price = bidsKeys[i]
                    const pp = BOOK.bids[price]
                    csdata.push(pp.price, pp.amount)
                }
                if (asksKeys[i]) {
                    const price = asksKeys[i]
                    const pp = BOOK.asks[price]
                    csdata.push(pp.price, -pp.amount)
                }
            }



        // handle book. create book or update/delete price points
        if (BOOK.mcnt === 0) {
            _.each(msg[1], function (pp) {
                pp = { price: pp[0], cnt: pp[1], amount: pp[2] }
                const side = pp.amount >= 0 ? 'bids' : 'asks'
                pp.amount = Math.abs(pp.amount)
                BOOK[side][pp.price] = pp
            })
        } else {
            msg = msg[1]
            const pp = { price: msg[0], cnt: msg[1], amount: msg[2] }

            // if count is zero, then delete price point
            if (!pp.cnt) {
                let found = true

                if (pp.amount > 0) {
                    if (BOOK['bids'][pp.price]) {
                        delete BOOK['bids'][pp.price]
                    } else {
                        found = false
                    }
                } else if (pp.amount < 0) {
                    if (BOOK['asks'][pp.price]) {
                        delete BOOK['asks'][pp.price]
                    } else {
                        found = false
                    }
                }

                if (!found) {
                    console.error('Book delete failed. Price point not found')
                }
            } else {
                // else update price point
                const side = pp.amount >= 0 ? 'bids' : 'asks'
                pp.amount = Math.abs(pp.amount)
                BOOK[side][pp.price] = pp
            }

            // save price snapshots. Checksum relies on psnaps!
            _.each(['bids', 'asks'], function (side) {
                const sbook = BOOK[side]
                const bprices = Object.keys(sbook)
                const prices = bprices.sort(function (a, b) {
                    if (side === 'bids') {
                        return +a >= +b ? -1 : 1
                    } else {
                        return +a <= +b ? -1 : 1
                    }
                })
                BOOK.psnap[side] = prices
            })
        }
        BOOK.mcnt++

    }}}