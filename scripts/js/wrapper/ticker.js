var tickerfunc = function(chanId){
    "use strict";
    var tickerpair;
    var tickerchan;
    var bid
    var bid_size
    var ask
    var daily_change
    var DAILY_CHANGE_PERC
    var tickerprice
    var currency = "$";
    var volume
    var high
    var low
    var pricevalid = response[7];

    var varsave_fillfrontend = function(response, tickerpair) {
        tickerpair = tickerpair;
        bid = response[1]
        bid_size = response[2]
        ask = response[3]
        daily_change = response[5]
        DAILY_CHANGE_PERC = response[6]
        tickerprice = response[7];
        currency = "$";
        volume = response[8]
        high = response[9]
        low = response[10]
        fillheatmap(tickerpair, tickerprice, currency, DAILY_CHANGE_PERC, daily_change, volume, high, low);
    }
    var tickermap = function(chanId) {
        $.each(TICKERMAP, function(key, value) {
            tickerchan = Object.values(value).toString()
            tickerpair = key;
            if (chanId == tickerchan) {
                if (pricevalid > 0) { //if message has a price on its position
                    varsave_fillfrontend(response, tickerpair);
                }
            }
        });
    }
    tickermap(chanId);
}