
function tradechannel(pair){

    var id = "Bitfinex_buysell"
    var buf = {};
    buf['Bitfinex_buysell'] = [[], []];
    var tradechan;
    var ws = new WebSocket('wss://api.bitfinex.com/ws/');
    console.log(pair)
    var price = 0;
    ws.onopen = function () {
        ws.send(JSON.stringify({      // send subscribe request
            "event": "subscribe",
            "channel": "trades",
            "pair": pair
        }));
    };
    ws.onmessage = function (msg) {     // callback on message receipt
        var response = JSON.parse(msg.data);

        if (response["pair"] == pair) {
            tradechan = response["chanId"]
            pair = response["pair"]

        }
        if (response[0] == tradechan)
            if (response[1] === 'te') {    // Only 'te' message type is needed
                timestamp = response[3]
                var timestampInMilliSeconds = timestamp*1000;
                var time = new Date(timestampInMilliSeconds);

                price = response[4]
                amount = response[5]

                trades(timestamp, price, amount, pair);

                buf['Bitfinex_buysell'][response[5] > 0 ? 0 : 1].push({
                    x: response[3] * 1000, // timestamp in milliseconds
                    y: response[4]         // price in US dollar
                });
            }
    }
}
