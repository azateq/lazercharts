

var ws3 = new WebSocket('wss://api.bitfinex.com/ws/');
// updates
/*
[
    "<CHANNEL_ID>",
    "<PRICE>",
    "<COUNT>",
    "<AMOUNT>"
]
 */
var id3 = "bitfinex_orderbook"
var buf3 = {};
buf3['bitfinex_orderbook'] = [[], []];
var price;

ws3.onopen = function() {
    ws3.send(JSON.stringify({ event: 'subscribe', channel: 'book', pair: 'tBTCUSD', prec: 'R0' }))
};
ws3.onmessage = function(msg) {     // callback on message receipt
    var response = JSON.parse(msg.data);

    price = response[2]
    if(response[3] >= 0 ){
        amountpos = response[3]
    }else{
        amountneg = response[3]
    }



 //   updateData(myChart, [response[3]], 1)
}
function addData(chart, data, datasetIndex) {
    chart.data.datasets[datasetIndex].data = data;
    chart.update();
}

var gradientStroke = 'rgba(0, 119, 220, 0.6)',
    gradientFill = 'rgba(0, 119, 220, 0.4)';

var ctx = document.getElementById(id3).getContext("2d");

var chart = new Chart(ctx, {
    type: 'line',

    data: {

        datasets: [{

            data: []

        },{

            data: []

        }]

    },

    options: {
        title: {
            text: 'BTC/USD (' + id3 + ')', // chart title
            display: true
        },
        scales: {
            xAxes: [{
                type: 'realtime'
            }],
            yAxes: [{
                position: 'right',

            }]
        },
        plugins: {

            streaming: {
                duration: 15000,
                onRefresh: function(chart) {
                    chart.data.datasets[0].data.push({
                        x: Date.now(),
                        y: amountpos
                    }),
                        chart.data.datasets[1].data.push({
                            x: Date.now(),

                            y: amountneg
                        })

                    amountneg = 0;
                    amountpos = 0;
                },
                delay: 2000

            }

        }

    }})

/*
setTimeout(function() {
    addData(chart, response, 0);
}, 2000);
 */


function addData(chart, data, datasetIndex) {
    chart.data.datasets[datasetIndex].data = data;
    chart.update();
}
function updateData(chart, data) {
    chart.data.datasets[0].data = data,
    chart.update();

}
function updateLabel(chart, label) {
    chart.data.labels = label,
    chart.update();
}
function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}

