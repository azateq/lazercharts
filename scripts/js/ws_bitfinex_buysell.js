var id = "Bitfinex_buysell"
var buf = {};
buf['Bitfinex_buysell'] = [[], []];
var tradechan_btcusd;
var tradechan_ltcusd;
var ws = new WebSocket('wss://api.bitfinex.com/ws/');

var price = 0;
ws.onopen = function () {
    ws.send(JSON.stringify({      // send subscribe request
        "event": "subscribe",
        "channel": "trades",
        "pair": "BTCUSD"
    }));
    ws.send(JSON.stringify({      // send subscribe request
        "event": "subscribe",
        "channel": "trades",
        "pair": "LTCUSD"
    }));
};
ws.onmessage = function (msg) {     // callback on message receipt
    var response = JSON.parse(msg.data);

    if (response["pair"] == "BTCUSD") {
        tradechan_btcusd = response["chanId"]
        btcpair = response["pair"]

    }
    if (response["pair"] == "LTCUSD") {
        tradechan_ltcusd = response["chanId"]
        ltcpair = response["pair"]
    }


    if (response[0] == tradechan_btcusd)
        if (response[1] === 'te') {    // Only 'te' message type is needed
            timestamp = response[3]
            var time = new Date(timestamp*1000);
            btcprice = response[4]
            btcamount = response[5]


            trades(time, btcprice, btcamount, btcpair);


            buf['Bitfinex_buysell'][response[5] > 0 ? 0 : 1].push({
                x: response[3] * 1000, // timestamp in milliseconds
                y: response[4]         // price in US dollar
            });
        }
    if (response[0] == tradechan_ltcusd)
        if (response[1] === 'te') {    // Only 'te' message type is needed
            time = response[3]
            ltcprice = response[4]
            ltcamount = response[5]


            ltcusdtrades(time, ltcprice, ltcamount, ltcpair);



        }

}



var ctx = document.getElementById(id).getContext('2d');

var chart = new Chart(ctx, {

    type: 'line',

    data: {

        datasets: [{
            data: [],
            label: 'Buy',
            spanGaps: false,
            cubicInterpolationMode: 'default',
            borderColor: 'red',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
            fill: false,
            lineTension: 0,                     // straight line
            pointRadius: 5                      // no point
        },{
            data: [],
            label: 'Sell',
            spanGaps: false,
            cubicInterpolationMode: 'monotone',
            borderColor: 'rgb(54, 162, 235)',
            backgroundColor: 'rgba(54, 162, 235, 0.5)',
            fill: false,
            lineTension: 0,                    // straight line
            pointRadius: 5                     // no point

        }]

    },


    options: {
        title: {
            text: 'BTC/USD (' + id + ')', // chart title
            display: true
        },
        scales: {

            xAxes: [{
                type: 'realtime' // auto-scroll on X axis
            }],
            yAxes: [{
                position: 'right',

            }]
        },
        plugins: {
            streaming: {
                duration: 30000, // display data for the latest 300000ms (5 mins)
                onRefresh: function(chart) { // callback on chart update interval
                    Array.prototype.push.apply(
                        chart.data.datasets[0].data, buf[id][0]
                    );            // add 'buy' price data to chart
                    Array.prototype.push.apply(
                        chart.data.datasets[1].data, buf[id][1]
                    );            // add 'sell' price data to chart
                    buf[id] = [[], []]; // clear buffer
                },
                delay: 1500
            }
        }

    }})





// Buy ad Sell trennen
/*


 */