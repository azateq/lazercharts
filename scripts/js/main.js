/*
$('#asks').scrollTop($('#asks')[0].scrollHeight - $('#asks')[0].clientHeight);

 */
/* Set the width of the side navigation to 250px */

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}
/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

$(document).ready(function () {



    $("#showtime").click(function() {

        if(!$(this).is(":checked")){
            $(".timestamp").removeClass('timestampshow');  // unchecked
        }else{
            $(".timestamp").addClass('timestampshow');  // unchecked
        }
    });



    var closeBtn = document.getElementById('close');
    var openBtn = document.getElementById('open');


    $('#close').click(function(e){
        e.preventDefault();
        pair = $(this).find('.pair').text();
        // Close the WebSocket.
        ws.send(JSON.stringify({      // send subscribe request
            "event": "unsubscribe",
            "chanId": orderbookchanId,
        }));
        ws.send(JSON.stringify({      // send subscribe request
            "event": "unsubscribe",
            "chanId": tradechanId,
        }));
        $("div.order-item").remove();
        $("div.trade-item").remove();
        return false;
    });

// Close the WebSocket connection when the close button is clicked.
    $('.BTCUSD, .LTCUSD, .ETHUSD, .EOSUSD, .XMRUSD, .ZECUSD, .NEOUSD ').click(function(e){
        e.preventDefault();
        pair = $(this).find('.pair').text();

        console.log(pair)
        // Close the WebSocket.
       //  ws.close();
        ws.send(JSON.stringify({      // send subscribe request
            "event": "unsubscribe",
            "chanId": orderbookchanId,
        }));
        /*
        ws.send(JSON.stringify({"event": "unsubscribe","chanId": BTCUSD_tickerchan,}));
        ws.send(JSON.stringify({"event": "unsubscribe","chanId": LTCUSD_tickerchan,}));
        ws.send(JSON.stringify({"event": "unsubscribe","chanId": ETHUSD_tickerchan,}));
         */
        ws.send(JSON.stringify({      // send subscribe request
            "event": "unsubscribe",
            "chanId": tradechanId,
        }));


        $("div.order-item").remove();
       // $("div.trade-item").remove();


            setTimeout(function() {
                resubscribe();
            }, 500);


        return false;
    });
    // OPEN the WebSocket connection when the open button is clicked.

        function resubscribe(){
            "use strict";
            ws.send(JSON.stringify({      // send subscribe request
                "event": "subscribe",
                "channel": "book",
                "pair": pair,
                "prec": "P2",
                "len": "25",
                "freq": "F0"
            }));
            snap = true;
            ws.send(JSON.stringify({      // send subscribe request
                "event": "subscribe",
                "channel": "trades",
                "pair": pair
            }));
            chart.update();
            createChart(pair);

            if(pair != "close")
              $('.pair_title').text(pair);

            return true;
        }



    var windowheight = $( window ).height()
    var widgetbarheight = $(".widget-bar").outerHeight()
    var footerheight = $(".footer-div").outerHeight()
    var amountchartheight = $("#Bitfinex").outerHeight();
    var filterheight = $(".filters").outerHeight()
    var BitfinexWrapper = $(".Bitfinex-wrapper").outerHeight();
    var tradetitleheight = $(".trade-list .title").outerHeight()

    var contentheight = windowheight-widgetbarheight-footerheight;
    $("#tv").height(amountchartheight);
    var tv = $("#tv").outerHeight();

    $(".chat-outer").height((contentheight-tv)/2-tradetitleheight);
    $(".liquidations-outer").height((contentheight-tv)/2-tradetitleheight);

    // $(".tradehistory").height(contentheight);
    //$(".orderbook").height(contentheight)
    // $(".tradehistory").height(contentheight-amountchartheight-filterheight)
    // $(".maincontentleft").height(contentheight)
    //$(".chat-outer").height((contentheight-tvchart-titleheight)/2-marginsize);
    // $(".liquidations-outer").height((contentheight-tvchart-titleheight)/2-marginsize);
    $("#complete").height(contentheight-widgetbarheight);


    $(".scrollme").height(contentheight-BitfinexWrapper-filterheight-tradetitleheight);



    /*
      var select = $("#select")[0];

        select.add(new Option("btcusd", 1));
        select.add(new Option("ltcusd", 2));
        select.add(new Option("ethusd", 3));

        $("#select").click(function () {
            var option_all = $("select option:selected").map(function () {
                return $(this).text();
            }).get().join(',');
            console.log(option_all);
            pair = option_all;
        });
    */    var themestring2 = "minimal"

/*
$('.chat').mCustomScrollbar({
        theme: themestring2,
        autoHideScrollbar: false,
        scrollInertia: 0,
        alwaysShowScrollbar: 0,
        mouseWheel:{ preventDefault: true, scrollAmount: 100 },
        live: true,
        scrollEasing:"linear",
        timeout: 0,

    });
 */

   $('.customscroll').mCustomScrollbar({
       theme: themestring2,
       mouseWheel:{ scrollAmount: 200 },
       autoHideScrollbar: true
   });


/*
 $( document.body ).click(function () {
        var element = "#tv"
        $( element ).hide();
        if ( $( element ).is( ":hidden" ) ) {
            $( element ).slideDown( "50" );
        } else {
            $( element ).slideUp();
        }
    });
 */

    $( "#tv" ).ready(function() {
       // $('.hidden').delay( 1000 ).slideDown(2000);

    });


    //$( ".data-div" ).delay(2000).slideUp( "slow" );

    $( ".widget-collapse" ).click(function() {
        $( ".data-div" ).slideToggle( "slow" );
    });

});

$(window).resize(function(){
    var windowheight = $( window ).height()
    var widgetbarheight = $(".widget-bar").outerHeight()
    var footerheight = $(".footer-div").outerHeight()
    var amountchartheight = $("#Bitfinex").outerHeight();
    var filterheight = $(".filters").outerHeight()
    var BitfinexWrapper = $(".Bitfinex-wrapper").outerHeight();
    var tradetitleheight = $(".trade-list .title").outerHeight()

    var contentheight = windowheight-widgetbarheight-footerheight;
    $("#tv").height(amountchartheight);
    var tv = $("#tv").outerHeight();

    $(".chat-outer").height((contentheight-tv)/2-tradetitleheight);
    $(".liquidations-outer").height((contentheight-tv)/2-tradetitleheight);

    // $(".tradehistory").height(contentheight);
    //$(".orderbook").height(contentheight)
    // $(".tradehistory").height(contentheight-amountchartheight-filterheight)
    // $(".maincontentleft").height(contentheight)
    //$(".chat-outer").height((contentheight-tvchart-titleheight)/2-marginsize);
    // $(".liquidations-outer").height((contentheight-tvchart-titleheight)/2-marginsize);
    $("#complete").height(contentheight-widgetbarheight);


    $(".scrollme").height(contentheight-BitfinexWrapper-filterheight-tradetitleheight);

})

// zeige timestamp in tradehistory --- binded on checkbox showtime -- html
